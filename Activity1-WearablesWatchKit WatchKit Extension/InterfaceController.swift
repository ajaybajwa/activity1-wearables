//
//  InterfaceController.swift
//  Activity1-WearablesWatchKit WatchKit Extension
//
//  Created by AJAY BAJWA on 2019-10-27.
//  Copyright © 2019 lambton. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    var btnCount = 0
    @IBOutlet weak var lblMsgFromPhone: WKInterfaceLabel!
    @IBOutlet weak var lblOutput: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("WATCH APP LOADED")
        
        if WCSession.isSupported() == true{
            self.lblMsgFromPhone.setText("WC SUPPORTED")
                // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            self.lblMsgFromPhone.setText("WC NOT supported!")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("watch recieved a message")
        
        let name = message["name"] as! String
        let color = message["color"] as! String
        
        self.lblMsgFromPhone.setText("Name: \(name) \nColor: \(color)")
    }
    
    @IBAction func btnClick() {
        self.btnCount = self.btnCount + 1
        
        if WCSession.default.isReachable == true{
            let message = ["name":"pritesh","age":20] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil, errorHandler: nil)
            print("Message sent to phone")
            self.lblOutput.setText("Message sent to Phone: \(self.btnCount)")
        }
        else{
            self.lblOutput.setText("Message not sent to Phone: \(self.btnCount)")
        }
        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
