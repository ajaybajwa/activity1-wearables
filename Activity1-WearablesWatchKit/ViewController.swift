//
//  ViewController.swift
//  Activity1-WearablesWatchKit
//
//  Created by AJAY BAJWA on 2019-10-27.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblSessionSupOrNot: UILabel!
    
    var count:Int = 0
    var msgCounter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if WCSession.isSupported() == true{
            self.lblSessionSupOrNot.text = "WCSession supported";
        
        // create a communication session with the watch
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            self.lblSessionSupOrNot.text = "WC NOT supported!"
        }
 
    print("Phone app launched")
    }

    @IBAction func btnClicked(_ sender: UIButton) {
        self.count = self.count + 1
        self.lblCounter.text = "Click: \(self.count)"
    }
    @IBAction func btnSendMsgToWatch(_ sender: Any) {
        if WCSession.default.isReachable == true{
            let message = ["name":"banana","color":"yellow"]
            WCSession.default.sendMessage(message, replyHandler: nil, errorHandler: nil)
            print("sending message to watch")
            self.msgCounter = self.msgCounter + 1
            self.lblCounter.text = "Message Number/Cout: \(self.msgCounter)"
        }
        else{
            print("sending message to watch failed")
            self.msgCounter = self.msgCounter + 1
            self.lblCounter.text = "Message not sent: \(self.msgCounter)"
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Phone Recieved a message")
        let name = message["name"] as! String
        let age = message["age"] as! Int
        self.lblSessionSupOrNot.text = "Name: \(name)\nAge: \(age)"
    }
    
}

